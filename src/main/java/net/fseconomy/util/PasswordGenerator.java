package net.fseconomy.util;

import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Collections;

/**
 * original code from Crunchify.com
 * https://crunchify.com/java-generate-strong-random-password-securerandom/
 * Best way to generate very secure random Password automatically
 *
 */

public class PasswordGenerator
{

    // SecureRandom() constructs a secure random number generator (RNG) implementing the default random number algorithm.
    private static SecureRandom RNG = new SecureRandom();
    private static ArrayList<Object> VALIDCHARACTERS;
    private static final int DEFAULT_PASSWORD_SIZE = 20;
    private static final String DEFAULT_ALLOWED_CHARACTERS = "a-zA-Z0-9.-_~!@#$^";

    // Just initialize ArrayList VALIDCHARACTERS and add ASCII Decimal Values
    static
    {
        //initialize the valid character array
        VALIDCHARACTERS = new ArrayList<>();

        // Adding ASCII Decimal values 0-9
        for (int i = 48; i <= 57; i++) {
            VALIDCHARACTERS.add((char) i);
        }

        // Adding ASCII Decimal values a-z
        for (int i = 65; i <= 90; i++) {
            VALIDCHARACTERS.add((char) i);
        }

        // Adding ASCII Decimal values A-Z
        for (int i = 97; i <= 122; i++) {
            VALIDCHARACTERS.add((char) i);
        }

        VALIDCHARACTERS.add((char) 33);  // !
        VALIDCHARACTERS.add((char) 35);  // #
        VALIDCHARACTERS.add((char) 36);  // $
        VALIDCHARACTERS.add((char) 45);  // -
        VALIDCHARACTERS.add((char) 46);  // .
        VALIDCHARACTERS.add((char) 64);  // @
        VALIDCHARACTERS.add((char) 94);  // ^
        VALIDCHARACTERS.add((char) 95);  // _
        VALIDCHARACTERS.add((char) 126); // ~

        // rotate() rotates the elements in the specified list by the specified distance. This will create strong password
        Collections.rotate(VALIDCHARACTERS, RNG.nextInt(VALIDCHARACTERS.size()));
    }

    //get new random password using default size
    public static String getNewPassword()
    {
        String pw;
        do
        {
            pw = "";

            for (int length = 0; length < DEFAULT_PASSWORD_SIZE; length++)
            {
                pw += getRandomChar();
            }
        }
        while(!Helpers.isPasswordValid(pw));

        return pw;
    }

    //public static String getAllowedCharacters()
    //{
    //    return Constants.;
    //}

    // Get Char value from above added Decimal values
    public static char getRandomChar()
    {
        char result = (char) VALIDCHARACTERS.get(RNG.nextInt(VALIDCHARACTERS.size()));

        return result;
    }
}
