package net.fseconomy.data;

import  net.fseconomy.beans.AnnouncementBean;
import java.util.List;
import java.sql.*;
import java.util.*;

public class Announcements
{

    public static void createEntry(AnnouncementBean a)  throws DataError
    {
        try
        {
            String qry = "INSERT INTO announcements (created, createdby, updated, updatedby, posted, title, body, active, deleted) VALUES(?,?,?,?,?,?,?,?,?)";
            DALHelper.getInstance().ExecuteNonQuery(qry,
                    a.getCreated(), a.getCreatedBy(),
                    a.getUpdated(), a.getUpdatedBy(),
                    a.getPosted(),
                    a.getTitle(), a.getBody(),
                    a.getActive(), a.getDeleted());

        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
    }

    public static void updateEntry(AnnouncementBean a) throws DataError
    {
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;
        try
        {
            conn = DALHelper.getInstance().getConnection();
            stmt = conn.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_UPDATABLE);
            rs = stmt.executeQuery("SELECT * FROM announcements WHERE id = " + a.getId());

            //we over-write everything but the Id so get that from the resultset
            if(rs.next())
            {
                a.setId(rs.getInt("id"));
                a.writeBean(rs);

                rs.updateRow();
                rs.close();
            }
            else
                throw new DataError("unable to find announcement id!");
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
        finally
        {
            DALHelper.getInstance().tryClose(rs);
            DALHelper.getInstance().tryClose(stmt);
            DALHelper.getInstance().tryClose(conn);
        }
    }

    public  static AnnouncementBean getAnnouncement(int id) throws DataError
    {
        AnnouncementBean result = null;
        try
        {
            String qry = "SELECT * FROM announcements WHERE id = ?";
            ResultSet rs = DALHelper.getInstance().ExecuteReadOnlyQuery(qry, id);

            if(rs.next())
            {
                result = new AnnouncementBean(rs);
            }
            else
            {
                throw new DataError("unable to find announcement id!");
            }
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }

        return result;
    }

    public static List<AnnouncementBean> getAnnouncements() throws DataError
    {
        List<AnnouncementBean> result = new ArrayList<>();

        try
        {
            String qry = "SELECT * FROM announcements WHERE deleted = false";
            ResultSet rs = DALHelper.getInstance().ExecuteReadOnlyQuery(qry);

            while(rs.next())
            {
                AnnouncementBean bean = new AnnouncementBean(rs);
                result.add(bean);
            }
        }
        catch (SQLException e)
        {
            e.printStackTrace();
            throw new DataError("There was an error!");
        }

        return result;
    }

    public static List<AnnouncementBean> getActiveAnnouncements() throws DataError
    {
        List<AnnouncementBean> result = new ArrayList<>();

        try
        {
            String qry = "SELECT * FROM announcements WHERE active and not deleted";
            ResultSet rs = DALHelper.getInstance().ExecuteReadOnlyQuery(qry);

            while(rs.next())
            {
                AnnouncementBean bean = new AnnouncementBean(rs);
                result.add(bean);
            }
        }
        catch (SQLException e)
        {
            e.printStackTrace();
            throw new DataError("There was an error!");
        }

        return result;
    }

    public static void deleteEntry(int id, int userId) throws DataError
    {
        try
        {
            //check if it exists
            String qry = "SELECT (id is not null) as found FROM announcements WHERE id = ?";
            boolean exists = DALHelper.getInstance().ExecuteScalar(qry, new DALHelper.BooleanResultTransformer(), id);

            if(!exists)
                throw new DataError("Could not find Id!");

            Timestamp ts = new Timestamp(GregorianCalendar.getInstance().getTime().getTime());
            qry = "UPDATE announcements set deleted=1, updated=?, updatedby=? WHERE id = ?";
            DALHelper.getInstance().ExecuteUpdate(qry,ts, userId, id);
        }
        catch (SQLException e)
        {
            e.printStackTrace();
            throw new DataError("There was an error!");
        }

    }
}
