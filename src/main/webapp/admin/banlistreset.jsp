<%@page language="java"
        contentType="text/html; charset=ISO-8859-1"
        import="net.fseconomy.data.*"
%>
<%@ page import="net.fseconomy.beans.UserBean" %>

<jsp:useBean id="user" class="net.fseconomy.beans.UserBean" scope="session" />

<%
    if (!Accounts.needLevel(user, UserBean.LEV_MODERATOR))
    {
%>
        <script type="text/javascript">document.location.href="index.jsp"</script>
<%
        return;
    }

    String message = null;
%>
<jsp:include flush="true" page="/head.jsp" />
    <script type="text/javascript" src="../scripts/AdminAutoComplete.js"></script>
    <script type="text/javascript">

        $(function()
        {
            initAutoComplete("#accountname", "#account", <%= Accounts.ACCT_TYPE_ALL %>);
        });

    </script>
</head>
<body>

<jsp:include flush="true" page="/top.jsp" />
<jsp:include flush="true" page="/menu.jsp" />

<div id="wrapper">
    <div class="content">

<%
    if (message != null)
    {
%>
        <div class="message"><%= message %></div>
<%
    }
%>

        <div class="form" style="width: 400px">
            <h2>Reset User Aircraft Rental Ban List</h2>


            <form method="post" action="/userctl">
                <div class="formgroup">
                    Account/Group to reset the ban list for:
                    <input id="accountname" name="accountname" type="text" class="textarea" size="65"/>
                    <input type="hidden" id="account" name="account" />
                    <br/>
                </div>

                <div class="formgroup">
                    <input type="submit" class="button" value="Reset Ban List" />
                    <input type="hidden" name="event" value="resetBanList"/>
                    <input type="hidden" name="return" value="/admin/banlistreset.jsp"/>
                </div>
            </form>
        </div>
    </div>
</div>
</body>
</html>
