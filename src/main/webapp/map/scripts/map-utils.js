var fseMapUtils = (function() {
    'use strict';

    const MarkerTypeEnum = {"Depart":1, "Dest":2}
    Object.freeze(MarkerTypeEnum);

    const contentTemplate = [
        '<div id="tabs">',
        '<ul>',
        '~TABNAMES~',
        '</ul>',
        '~TABCONTENT~',
        '</div>'
    ];


    function createMapMarker(map, iconType, point) {
        //Default icon type
        var iconImage = "img/iconac.png";

        switch (iconType) {
            case IconTypeEnum.Depart:
                iconImage = "img/iconac.png";
                break;
            case IconTypeEnum.Dest:
                iconImage = "img/icondest.png";
                break;
            //default:
            //    break;
        }

        var marker = new google.maps.Marker({
            position: point,
            map: map,
            icon: {
                url: iconImage,
                size: new google.maps.Size(12, 20)
            }
        });

        return marker;
    }

    function getMapCenter(point1, point2) {
        var bounds = new google.maps.LatLngBounds(point1, point2);

        // get center point of bounds
        var clat = (bounds.getNorthEast().lat() + bounds.getSouthWest().lat()) / 2;
        var clng = (bounds.getNorthEast().lng() + bounds.getSouthWest().lng()) / 2;
        var mapCenter = google.maps.LatLng(clat, clng);

        return mapCenter;
    }

    //tab model
    function tab(name, content) {
        this.name = name;
        this.content = content;
    }

    function createTabName(title, tabNumber) {
        return '<li><a href="#tab_' + tabNumber + '"><span>' + title + '</span></a></li>';
    }

    function createTabContent(content, tabNumber) {
        return '<div id="tab_' + tabNumber + '"><p>' + content + '</p></div>';
    }


    //marker is the target to add the provided tab data to the global infowindow
    //tabs is an array of tab models
    function createMarkerTabContent(marker, tabs, infoWindow) {
        if (!marker || !tabs || !infoWindow) return;

        var c = contentTemplate.slice(0); //deep copy for strings
        var tabCount = 0;
        var out = "";

        tabs.forEach(t => {
                    c[2] = createTabName(t.name, tabCount);
                    c[4] = createTabContent(t.content, tabCount);
                    out = c.join('');
                });
        infoWindowGlobal.setContent(out);
    }

    return {
        createMarkerTabContent: createMarkerTabContent(),
        tab: tab(),
        getMapCenter: getMapCenter(),
        createMapMarker: createMapMarker(),
        MarkerTypeEnum: MarkerTypeEnum
    };
}());
