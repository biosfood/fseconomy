<%@page language="java"
        contentType="text/html; charset=ISO-8859-1"
        import="java.util.*, org.apache.commons.lang3.math.*, net.fseconomy.beans.*, net.fseconomy.dto.*, net.fseconomy.data.*, net.fseconomy.util.*"
%>

<%@ page import="static java.lang.Math.abs" %>

<jsp:useBean id="user" class="net.fseconomy.beans.UserBean" scope="session"/>

<%
    if (user == null || !user.isLoggedIn())
    {
%>
<script type="text/javascript">document.location.href = "/index.jsp"</script>
<%
        return;
    }

    //setup return page if action used
    String returnPage = "airportsearch.jsp";

    String icao = (String) request.getSession().getAttribute("airportSearchIcao");
    String pageNum = (String) request.getSession().getAttribute("airportSearchPage");
    List<CachedAirportBean> list = (List<CachedAirportBean>) request.getSession().getAttribute("airportSearchList");
%>
<jsp:include flush="true" page="/head.jsp" />
    <script>
        var selectedType = "";
        function onTypeChecked() {
            var airportSection = document.getElementById("airportSection");
            var aircraftSection = document.getElementById("aircraftSection");

            selectedType = document.querySelector('input[name="searchType"]:checked').value;
            airportSection.hidden = selectedType !== 'airport';
            aircraftSection.hidden = selectedType !== 'aircraft';
        }

        function onChecked() {
            if (!document.getElementById("event")) return;

            var el = document.getElementById("textEntry");
            var teLabel = document.getElementById("textEntryLabel");
            var elAC = document.getElementById("acTextEntry");
            var termSection = document.getElementById("termSection");
            var commoditySection = document.getElementById("commoditySection");
            var apDistanceSection = document.getElementById("apDistanceSection");
            var acTermSection = document.getElementById("acTermSection");
            var modelSection = document.getElementById("modelSection");
            var selectedAP= document.querySelector('input[name="searchAP"]:checked').value
            var selectedAC = document.querySelector('input[name="searchAC"]:checked').value

            termSection.hidden = true;
            commoditySection.hidden = true;
            apDistanceSection.hidden = true;
            acTermSection.hidden = true;
            modelSection.hidden = true;

            switch (selectedAP) {
                case "icao":
                    termSection.hidden = false;
                    teLabel.innerText = "Search For:";
                    el.setAttribute("maxlength", 4);
                    el.setAttribute("size", 4);
                    break;
                case "repairshop": // hide everything
                    apDistanceSection.hidden = false;
                    break;
                case "sell":
                case "buy":
                    teLabel.innerText = "ICAO:";
                    commoditySection.hidden = false;
                    apDistanceSection.hidden = false;
                    break;
                case "name":
                    teLabel.innerText = "Search For:";
                    termSection.hidden = false;
                    el.setAttribute("maxlength", 20);
                    el.setAttribute("size", 20);
                    break;
            }

            switch (selectedAC) {
                case "reg":
                    acTermSection.hidden = false;
                    elAC.setAttribute("maxlength", 8);
                    elAC.setAttribute("size", 8);
                    break;
                case "model":
                    modelSection.hidden = false;
                    break;
            }
        }

        function onDistanceCheckedChecked() {
            if (!document.getElementById("event")) return;

            var checked = document.getElementById("acDistanceChecked").checked;
            var acdistanceCheckedSection = document.getElementById("acDistanceCheckedSection");

            acdistanceCheckedSection.hidden = !checked;
        }

        function formValidation() {

            //submit hidden form values
            var searchBy = document.getElementById("searchBy");
            var searchFor = document.getElementById("searchFor");
            var searchTerm = document.getElementById("searchTerm");
            var modelId = document.getElementById("modelId");
            var distanceChecked = document.getElementById("distanceChecked");
            var distance = document.getElementById("distance");
            var subSearch = document.getElementById("subSearch");

            searchBy.value = selectedType;

            //airport validations
            if(selectedType === 'airport') {
                searchBy.value = selectedType;

                var textEntry = document.getElementById("textEntry");
                var selectedAP = document.querySelector('input[name="searchAP"]:checked');
                var comType = document.querySelector('input[name="comType"]:checked');
                var apIcao = document.getElementById("apIcao");
                var apDistance = document.getElementById("apDistance");

                // search by icao and name text
                if (selectedAP.value === 'icao' || selectedAP.value === 'name') {
                    if (!textEntry.value || textEntry.value.length < 3) {
                        window.alert("Please enter a term to search for! (3 character minimum");
                        textEntry.focus();

                        return false;
                    }
                    searchFor.value = selectedAP.value;
                    searchTerm.value = textEntry.value;
                }

                // search by commodity sell or buy
                if (selectedAP.value === 'repairshop') {
                    if (!apIcao.value || apIcao.value.length < 3 || apIcao.value.length > 4) {
                        window.alert("ICAO must be between 3 and 4 characters!");
                        apIcao.focus();

                        return false;
                    }
                    searchFor.value = selectedAP.value;
                    searchTerm.value = apIcao.value;
                    distance.value = apDistance.value;
                }

                // search by commodity sell or buy
                if (selectedAP.value === 'sell' || selectedAP.value === 'buy') {
                    if (!comType.value) {
                        window.alert("Please select commodity to search for!");

                        return false;
                    }

                    if (!apIcao.value || apIcao.value.length < 3) {
                        window.alert("ICAO must be at least 3 characters!");
                        apIcao.focus();

                        return false;
                    }
                    searchFor.value = selectedAP.value;
                    subSearch.value = comType.value;
                    searchTerm.value = apIcao.value;
                    distance.value = apDistance.value;
                }
            }
            else if(selectedType === 'aircraft') {

                //Entry fields
                var acTextEntry = document.getElementById("acTextEntry");
                var model = document.getElementById("model");
                var selectedAC = document.querySelector('input[name="searchAC"]:checked');
                var acdistanceChecked = document.getElementById("acdistanceChecked");
                var acIcao = document.getElementById("acIcao");
                var acDistance = document.getElementById("acDistance");
                var modelMode = document.querySelector('input[name="acType"]:checked');

                if(selectedAC.value === 'reg') {
                    if (acTextEntry.value.length < 3) {
                        window.alert("Search text must be at least 3 characters!");
                        acTextEntry.focus();
                        return false;
                    }
                    searchFor.value = selectedAC.value;
                    searchTerm.value = acTextEntry.value;
                }

                if (selectedAC.value === "model") {
                    if (!model.value) {
                        window.alert("Please select aircraft model to search for!");
                        model.focus();
                        return false;
                    }

                    if (acdistanceChecked.checked && (!acIcao.value || acIcao.value.length < 3)) {
                        window.alert("ICAO must be at least 3 characters!");
                        dfIcao.focus();

                        return false;
                    }

                    searchFor.value = selectedAC.value;
                    searchTerm.value = acIcao.value;
                    modelId.value = model.value;
                    distanceChecked.value = acdistanceChecked.checked;
                    distance.value = acDistance.value;
                    subSearch.value = modelMode.value;
                }
            }

            return true;
        }


        $(function () {

            $.extend($.tablesorter.defaults, {
                widthFixed: false,
                widgets: ['zebra', 'columns']
            });

            $('.airportSearchTable').tablesorter();

        });
    </script>
</head>
<body>

<jsp:include flush="true" page="top.jsp"/>
<jsp:include flush="true" page="menu.jsp"/>

<div class="content">
    <%
        String message = Helpers.getSessionMessage(request);
        if (message != null)
        {
    %>
    <div class="error">
        <%= message %>
    </div>
    <%
        }

        if (list == null)
        {
    %>
    <div class="container">

        <div class="container">
            <h1>Search By:</h1>

            <div class="form-group form-inline">
                <span style="margin-left: 20px;"> </span>
                <div class="checkbox">
                    <h2>
                        <label for="byAirport"><input type="radio" name="searchType" id="byAirport"
                                                      onchange="onTypeChecked()" value="airport" checked/>
                            Airport</label>
                    </h2>
                </div>
                <span style="margin-left: 20px;"> </span>
                <div class="checkbox">
                    <h2>
                        <label for="byAircraft"><input type="radio" name="searchType" id="byAircraft"
                                                       onchange="onTypeChecked()" value="aircraft"/> Aircraft</label>
                    </h2>
                </div>
            </div>

            <form class="panel panel-info form-horizontal col-xs-12 col-sm-8 col-l-6 col-xl-4" method="post" action="userctl"
                  onsubmit="return formValidation()">
                <input type="hidden" id="event" name="event" value="airportSearch"/>
                <input type="hidden" id="searchBy" name="searchBy" value=""/>
                <input type="hidden" id="searchFor" name="searchFor" value=""/>
                <input type="hidden" id="searchTerm" name="searchTerm" value=""/>
                <input type="hidden" id="modelId" name="modelId" value=""/>
                <input type="hidden" id="distanceChecked" name="distanceChecked" value=""/>
                <input type="hidden" id="distance" name="distance" value=""/>
                <input type="hidden" id="subSearch" name="subSearch" value=""/>
                <input type="hidden" id="page" name="page" value="1"/>
                <input type="hidden" name="returnpage" value="<%=returnPage%>"/>

                <div id="airportSection" class="form-group">
                    <div class="checkbox">
                        <label for="byicao"><input type="radio" name="searchAP" id="byicao" onchange="onChecked()"
                                                   value="icao" checked/> ICAO (Exact Match)</label>
                    </div>
                    <div class="checkbox">
                        <label for="byname"><input type="radio" name="searchAP" id="byname" onchange="onChecked()"
                                                   value="name"/> Name or City (contains match)</label>
                    </div>
                    <div class="checkbox">
                        <label for="hasRepairshop"><input type="radio" name="searchAP" id="hasRepairshop"
                                                          onchange="onChecked()" value="repairshop"/> Has Repair Shop</label>
                    </div>
                    <div class="checkbox">
                        <label for="sellCommodity"><input type="radio" name="searchAP" id="sellCommodity"
                                                          onchange="onChecked()" value="sell"/> Sells
                            (Commodities)</label>
                    </div>
                    <div class="checkbox">
                        <label for="buyCommodity"><input type="radio" name="searchAP" id="buyCommodity"
                                                         onchange="onChecked()" value="buy"/> Buys
                            (Commodities)</label>
                    </div>
                    <div id="termSection">
                        <div class="form-group">
                            <label id="textEntryLabel" class="control-label col-sm-3" for="textEntry">Search for:</label>
                            <div class="col-sm-7">
                                <input id="textEntry" name="textEntry" type="text" class="form-control" maxlength="4"
                                       size="4"/>
                            </div>
                        </div>
                    </div>

                    <div id="commoditySection">
                        <label class="control-label col-sm-3" for="textEntry">Commodity:</label>
                        <div class="col-sm-7">
                            <div class="checkbox">
                                <label for="byBuildingMats"><input type="radio" name="comType" id="byBuildingMats"
                                                                   value="buildingMats" checked/> Building
                                    Material</label>
                            </div>
                            <div class="checkbox">
                                <label for="bySupplies"><input type="radio" name="comType" id="bySupplies"
                                                               value="supplies" checked/> Supplies</label>
                            </div>
                            <div class="checkbox">
                                <label for="by100ll"><input type="radio" name="comType" id="by100ll" value="100ll"/>
                                    100LL</label>
                            </div>
                            <div class="checkbox">
                                <label for="byJetA"><input type="radio" name="comType" id="byJetA" value="jetA"/>
                                    Jet A</label>
                            </div>
                        </div>
                    </div>

                    <div id="apDistanceSection">
                        <label class="control-label col-sm-3"></label>
                        <div class="col-sm-7">
                            <div class="form-group">
                                <label class="control-label col-sm-3" for="apDistance">Within (NM):</label>
                                <div class="col-sm-7">
                                    <select id="apDistance" name="apDistance" class="form-control">
                                        <option class="formselect" value="10">10</option>
                                        <option class="formselect" value="20">20</option>
                                        <option class="formselect" value="50">50</option>
                                        <option class="formselect" value="100">100</option>
                                        <option class="formselect" value="250">250</option>
                                        <option class="formselect" value="500">500</option>
                                        <option class="formselect" value="1000">1000</option>
                                        <option class="formselect" value="2000">2000</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-sm-3" for="apIcao">Of (ICAO):</label>
                                <div class="col-sm-7">
                                    <input id="apIcao" name="apIcao" type="text" class="form-control"
                                           maxlength="4"
                                           size="4"/>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>


                <div id="aircraftSection" class="form-group" style="padding-left: 20px;">
                    <div class="radio">
                        <label for="byReg"><input type="radio" name="searchAC" id="byReg" value="reg"
                                                  onchange="onChecked()" checked/> Registration (Exact)</label>
                    </div>
                    <div class="radio">
                        <label for="byModel"><input type="radio" name="searchAC" id="byModel" value="model"
                                                    onchange="onChecked()"/> Make / Model</label>
                    </div>

                    <div id="acTermSection">
                        <div class="form-group">
                            <label class="control-label col-sm-3" for="acTextEntry">Search for:</label>
                            <div class="col-sm-7">
                                <input id="acTextEntry" name="acTextEntry" type="text" class="form-control"
                                       maxlength="4"
                                       size="4"/>
                            </div>
                        </div>
                    </div>


                    <div id="modelSection">
                        <label class="control-label col-sm-3" for="acTextEntry">Make / Model:</label>
                        <div class="col-sm-7">
                            <select  class="form-control" id="model" name="model">
                                <%
                                    List<ModelBean> models = Models.getAllModels();

                                    for (ModelBean model : models)
                                    {
                                %>
                                <option class="formselect" value="<%= model.getId() %>"><%= model.getMakeModel() %>
                                </option>
                                <%
                                    }
                                %>
                            </select>
                        </div>
                        <div class="col-sm-7">
                            <div class="radio">
                                <label for="byRent"><input type="radio" name="acType" id="byRent" value="rentable"
                                                           checked/> Rentable</label>
                            </div>
                            <div class="radio">
                                <label for="byFerry"><input type="radio" name="acType" id="byFerry" value="ferry"/>
                                    Ferry</label>
                            </div>
                            <div class="radio">
                                <label for="byForSale"><input type="radio" name="acType" id="byForSale"
                                                              value="forSale"/> For Sale</label>
                            </div>
                            <div class="checkbox">
                                <label for="acDistanceChecked">
                                    <input type="checkbox" id="acDistanceChecked" name="acDistanceChecked"
                                           onchange="onDistanceCheckedChecked()" value="value">
                                    Distance From
                                </label>
                            </div>
                        </div>

                        <div id="acDistanceCheckedSection">
                            <label class="control-label col-sm-3" for="textEntry"></label>
                            <div class="col-sm-7">
                                <div class="form-group">
                                    <label class="control-label col-sm-3" for="acIcao">ICAO:</label>
                                    <div class="col-sm-7">
                                        <input id="acIcao" name="acIcao" type="text" class="form-control"
                                               maxlength="4"
                                               size="4"/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-3" for="acDistance">Distance:</label>
                                    <div class="col-sm-7">
                                        <select id="acDistance" name="comDistance" class="form-control">
                                            <option class="formselect" value="10">10</option>
                                            <option class="formselect" value="20">20</option>
                                            <option class="formselect" value="50">50</option>
                                            <option class="formselect" value="100">100</option>
                                            <option class="formselect" value="250">250</option>
                                            <option class="formselect" value="500">500</option>
                                            <option class="formselect" value="1000">1000</option>
                                            <option class="formselect" value="2000">2000</option>
                                        </select>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group ">
                    <input type="submit" class="button form-control input-block-level" value="Search"/>
                </div>
            </form>
        </div>

        <%
            }
        %>
        <%--<div class="formgroup">--%>
        <%--Airports that have:--%>
        <%--<table style="margin-left:20px">--%>
        <%--<tr>--%>
        <%--<td><input type="checkbox" name="hasFuel" value="fuel" <%= hasFuel ? "checked" : ""%>/>--%>
        <%--100LL fuel--%>
        <%--</td>--%>
        <%--<td><input type="checkbox" name="hasJeta" value="jetafuel" <%= hasJeta ? "checked" : ""%>/>--%>
        <%--JetA fuel--%>
        <%--</td>--%>
        <%--<td><input type="checkbox" name="hasRepair"--%>
        <%--value="repair" <%= hasRepair ? "checked" : ""%>/> repairshop--%>
        <%--</td>--%>
        <%--</tr>--%>
        <%--<tr>--%>
        <%--<td><input type="checkbox" name="assignments"--%>
        <%--value="assignments" <%= hasAssignments ? "checked" : ""%>/> assignments--%>
        <%--</td>--%>
        <%--<td><input type="checkbox" name="ferry" value="ferry" <%= ferry ? "checked" : ""%>/>--%>
        <%--aircraft that need ferrying--%>
        <%--</td>--%>
        <%--</tr>--%>
        <%--<tr>--%>
        <%--<td><input type="checkbox" name="hasAcForSale"--%>
        <%--value="ac" <%= hasAcForSale ? "checked" : ""%>/> aircraft for sale--%>
        <%--</td>--%>
        <%--<td><input type="checkbox" name="hasFbo" value="fbo" <%= hasFbo ? "checked" : ""%>/> FBO--%>
        <%--</td>--%>
        <%--</tr>--%>
        <%--</table>--%>
        <%--</div>--%>
        <%--<div class="formgroup">--%>
        <%--Airports that are within--%>
        <%--<select name="distance" class="formselect">--%>
        <%--<option class="formselect" value="10">10</option>--%>
        <%--<option class="formselect" value="20">20</option>--%>
        <%--<option class="formselect" value="50" >50</option>--%>
        <%--<option class="formselect" value="100">100</option>--%>
        <%--<option class="formselect" value="250" >250</option>--%>
        <%--<option class="formselect" value="500" >500</option>--%>
        <%--<option class="formselect" value="1000" >1000</option>--%>
        <%--<option class="formselect" value="2000" >2000</option>--%>
        <%--</select>--%>
        <%--NM from--%>
        <%--<input name="from" type="text" class="textarea" value="" size="4"/>--%>
        <%--</div>--%>
        <%--<div class="formgroup">--%>
        <%--Airports that--%>
        <%--<select name="goodsMode" class="formselect">--%>
        <%--<option class="formselect" value="sell">Sell</option>--%>
        <%--<option class="formselect" value="buy">Buy</option>--%>
        <%--</select>--%>
        <%--<select name="commodity" class="formselect">--%>
        <%--<option class="formselect" value=""></option>--%>
        <%--<%--%>
        <%--for (int c = 0; c < Goods.commodities.length; c++)--%>
        <%--{--%>
        <%--if (Goods.commodities[c] == null)--%>
        <%--continue;--%>
        <%--%>--%>
        <%--<option class="formselect" value="<%= c %>"><%= Goods.commodities[c].getName() %>--%>
        <%--</option>--%>
        <%--<%--%>
        <%--}--%>
        <%--%>--%>
        <%--</select>--%>
        <%--Minimum Kg: <input name="minAmount" type="text" value="100" size="6"/>--%>
        <%--</div>--%>
        <%--<div class="formgroup">--%>
        <%--<input type="hidden" name="submit" value="true"/>--%>
        <%--<input type="submit" class="button" value="Go"/>--%>
        <%--<input type="reset" class="button" value="Clear"/>--%>
        <%--</div>--%>
        </form>
    </div>
    <%

        if (list != null)
        {
            //clear list
            request.getSession().setAttribute("airportSearchIcao", null);
            request.getSession().setAttribute("airportSearchList", null);
    %>
    <div>
        <h4><a href="airportsearch.jsp">Back to search</a></h4>
    </div>
    <div class="dataTable">
        <table class="airportSearchTable tablesorter-default tablesorter">
            <caption>Search Results - <%=list.size()%> Found</caption>
            <thead>
            <tr>
                <th>ICAO</th>
                <% if(icao != null) { %>
                <th>Nm</th>
                <th>Brg</th>
                <%}%>
                <th>Title</th>
                <th>Name</th>
                <th>Country</th>
            </tr>
            </thead>
            <tbody>
            <%
                for (CachedAirportBean bean : list)
                {

                    DistanceBearing dto = new DistanceBearing(0,0);
                    if(icao != null)
                    {
                        dto = Airports.getDistanceBearing(icao, bean.getIcao());
                    }
            %>
            <tr>
                <td><a class="normal"
                       href="<%= response.encodeURL("airport.jsp?icao=" + bean.getIcao()) %>"><%= bean.getIcao() %>
                </a></td>
                <% if(icao != null) { %>
                <td><%=Formatters.oneDigit.format(dto.distance)%></td>
                <td><%=Formatters.threeDigits.format(dto.bearing+.5)%></td>
                <%}%>
                <td><%= bean.getTitle() %>
                </td>
                <td><%= bean.getName() %>
                </td>
                <td><%= bean.getCountry() %>
                </td>
            </tr>
            <%
                }
            %>
            </tbody>
        </table>
    </div>
    <%
        }
    %>
</div>

<div class="footer">
</div>
<script>
    onTypeChecked();
    onChecked();
    onDistanceCheckedChecked();
</script>
</body>
</html>