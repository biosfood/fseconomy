<%@page language="java"
        contentType="text/html; charset=ISO-8859-1"
        import = "net.fseconomy.data.*"
%>
<%@ page import="net.fseconomy.beans.AnnouncementBean" %>
<%@ page import="java.util.List" %>
<%@ page import="net.fseconomy.util.Formatters" %>

<jsp:useBean id="user" class="net.fseconomy.beans.UserBean" scope="session" />

<%
    int parked = 0;
    int flying = 0;
	String xpVersion = "[TBD]";
    try
    {
        flying = Stats.getInstance().getNumberOfUsers("flying");
        parked = Stats.getInstance().getNumberOfUsers("parked");
		xpVersion = Data.getXPVersion();
    }
    catch(DataError e)
    {

    }
%>

<jsp:include flush="true" page="head.jsp" />

    <script type="text/javascript">

        //Original javascript code by Daniel Insley at bravenet.com - modified by Paul Dahlen.
        function createtime()
        {
            var time = new Date();
            var gmtMS = time.getTime() + (time.getTimezoneOffset() * 60000);
            var gmtTime =  new Date(gmtMS);
            var zhours = gmtTime.getHours();
            var tfzhours = zhours;
            var lhours = time.getHours();
            var tflhours = lhours;
            var minutes = gmtTime.getMinutes();
            var seconds = gmtTime.getSeconds();
            var abbrev = "AM";
            var labbrev = "AM";

            if (tfzhours < 10)
                tfzhours = "0"+tfzhours;

            if (tflhours < 10)
                tflhours = "0"+tflhours;

            if (zhours>=12)
                abbrev="PM";

            if (zhours>12)
                zhours=zhours-12;

            if (zhours==0)
                zhours=12;

            if (lhours>=12)
                labbrev="PM";

            if (lhours>12)
                lhours=lhours-12;

            if (lhours==0)
                lhours=12;

            if (minutes<=9)
                minutes="0"+minutes;

            if (seconds<=9)
                seconds="0"+seconds;

            var ctime=" &nbsp; "+"UTC: "+zhours+":"+minutes+":"+seconds+" "+abbrev+"  ("+tfzhours+":"+minutes+":"+seconds+") &nbsp;";

            if(document.all)
            {
                document.all.clock.innerHTML=ctime;
            }
            else if (document.getElementById)
            {
                document.getElementById("clock").innerHTML=ctime;
            }
            else
            {
                document.write(tftime);
                document.write(ctime);
            }
        }

        if (!document.all && !document.getElementById)
            createtime();

        function loadtime()
        {
            if (document.all || document.getElementById)
                setInterval("createtime()",1000);
        }

        // Popup Windows
        var team = new PopupWindow();

    </script>

<body onload="loadtime()" class="homepage">

<jsp:include flush="true" page="top.jsp" />
<jsp:include flush="true" page="menu.jsp" />

<div id="wrapper">
	<div class="content">
		<div class="news part1">
		<!-- MAIN PAGE CONTENT ======================================================= -->
	
		<!-- NEWS ITEM Logged Out Only -->
	<%
		if (!user.isLoggedIn()) 
		{ 
	%>  
			<section>
				<div>
					<h3>Welcome to the exciting world of <em>FSEconomy</em></h3>
					<p>FSEconomy is a environment where flight simulation enthusiasts can add a new aspect to their flying. Since 2005, FSEconomy has registered over 10,000 simulator pilots to earn in-game virtual money by flying general aviation aircraft to and from nearly every airport on Earth. With those virtual earnings, pilots can purchase their own airplanes, join or start virtual businesses, open up FBOs, and more - all within the free world of FSEconomy. FSEconomy adds a new dimension to your flight simulation experience, and that doesn't even count all the people you will meet along the way.</p>
					<p>If you would like to know more about this free community, just read the manual and visit our community forum. Links can be found below.</p>
				</div>
			</section>

			<section>
				<div>
					<h3>FSEconomy is FSX and X-Plane Compatible</h3>
					<p>In addition to Microsoft Flight Simulator 2004 and FSX, FSEconomy is also available to <a href="http://www.x-plane.com" target="_blank">X-Plane</a> users. Read more about it in the FSEconomy Manual.</p> 
				</div>
			</section>
	<% 	} 
	%>
		<!-- END NEW ITEM -->
	
		<!-- NEWS ITEM Logged In Only -->
	<%
		if (user.isLoggedIn())
		{
	%>
	<%
		//New announcements section
		List<AnnouncementBean> announcements = Announcements.getActiveAnnouncements();
	
	%>
	<%
		for (AnnouncementBean a : announcements)
		{
	%>
			<section>
				<div>
					<h3><%=a.getTitle()%></h3>
					<p>
						<%= Formatters.datemmddyy.format(a.getPosted())%><br>
						<%=a.getBody()%>
					</p>
				</div>
			</section>
	<%
		}
	}
	%>
		</div><!-- closes NEWS -->
	
		<!-- SHORTCUTS BLOCK ======================================================= -->
		<div class="home-shortcuts">
			<section>
			<div>
				<h4>Shortcuts</h4>
				<ul>
				<%
					if (!user.isLoggedIn()) 
					{
				%>
					<li><a href="http://www.fseconomy.net" target='_blank'>Sign Up For New Account</a></li>
				<%	
					}
				%>		
					<li>Read the <a target='_blank' href="https://sites.google.com/site/fseoperationsguide/">Manual</a></li>
					<li>Go to the <a target='_blank' href="http://www.fseconomy.net">FSE Community</a></li>
					<li><a href="https://sites.google.com/site/fseoperationsguide/getting-started/using-the-fse-client" target='_blank'>Installing the Clients</a></li>
					<li>Join us on <a href="https://www.fseconomy.net/forum/teamspeak/41-teamspeak-instructions-read-this-first" target='_blank'>FSE TeamSpeak</a></li>
					<li><a href="https://www.fseconomy.net/forum/fse-anouncements/91383-launching-the-official-fse-discord-server" target="_blank">FSE Discord</a></li>
					<li class="social-media">Follow us on your favorite social media:
						<ul>
							<li>
								<a href="https://www.facebook.com/FSEconomy">
									<img src="https://sites.google.com/site/fseoperationsguide/FSE-fb.png" />
								</a>
							</li>
							<li>
								<a  href="https://twitter.com/FSEconomy">
									<img src="https://sites.google.com/site/fseoperationsguide/fse-twitter.png" />
								</a>
							</li>
							<li>
								<a  href="https://www.youtube.com/fseconomy">
									<img src="https://sites.google.com/site/fseoperationsguide/FSE-youtube.png" />
								</a>
							</li>
						</ul>
					</li>
				</ul>

				<h4>Latest X-Plane Release</h4>
				<div>
					<div>Version: <%=xpVersion%></div>
					<div>Note: Previous versions can be deactivated at any time. Update your script following the [Installing the Clients] link above.</div>
				</div>	

				<h4>Pilot and Activity Map</h4>
				<div style="display: table; margin: 0 auto;">
					<a href="http://server.fseconomy.net/static/html/index.html" target="_blank">
						<img style="border-style: none; box-shadow: 5px 5px 5px #888888;" src="/img/pilotactivitymap.png">
					</a>	
				</div>
				<div style="display: table; margin: 15px auto; border-style:inset; font-size: 10pt; font-family: Arial,sans-serif;" id="clock" ></div>
				<a style="display: table; margin: 0 auto;" href="http://vatsim.net" target='_blank'><img style="border-style: none;" src="img/VATSIM_logo_small.gif" width="90" /></a>
				<div style="display: table; margin: 0 auto; font-size:x-small"><b>FSEconomy is a proud <a href="http://vatsim.net" onclick="this.target='_blank'">VATSIM</a> partner.</b></div>
			</div>
			</section>
		</div>
		<!-- END of shortcuts -->
		
		<!-- START of 2nd news area -->
		<div class="news part2">
<%
	if (user.isLoggedIn())
	{
%>
			<section>
				<div>
					<h3>Update - XPlane Client scripts updated to 1.9.X</h3>
					<h4 style="background-color: lightcoral">Note: Check the manual for the latest release as this could be outdated when you read it.</h4>
					<p>X-Plane client has been updated, it should fix several issues. Update at your earliest conveniance as the previous version will be removed after 30 days.<br><br>

						Changes from 1.9.0 to 1.9.1<br>
						-Corrected some encoding issues<br><br>
						Changes from 1.8.4 to 1.9.0:<br>
						-Updated functions that detect X-Plane Version to include XP11<br>
						-Fixed issue with uninitialized variable in XFSEpost<br>
						-Converted password and aircraft alias info into URL-encoding when passed to server (allowing for some special characters in passwords to work now)<br>
						-Changed "Clearing engine" text in logs to show engine number<br>
						-Flight Time is now a 'float' instead of an 'int' for more accurate calculations<br>
						-Adjusted engine damage check to be based on time passed and not a static '1 second'<br>
						-Added handling for dom parser errors<br>
						-Added FlyWithLua interface README.txt file<br>
						-Added check for aircraft movement speeds based on lat/long math to ensure no teleporting<br>
						-Added current date/time to logs<br>
						-Added X-Plane Version to logs<br>
						-Added ability to end flight using parking brake<br>
						-Updated FlyWithLua and python-xfse interfaces to work with parking brake end flight option<br>
						-Added the ability to use ground acceleration<br>
					</p>
				</div>
			</section>
<% 	}
%>

	<!-- NEWS ITEM Logged In Only -->
<%
	if (user.isLoggedIn())
	{
%>
		<section>
			<div>
				<h3>Update - Aircraft Ownership Fees</h3>
				<p>FSE is implementing a monthly "Aircraft Ownership Expenses Fee" for all privately-owned aircraft. We are also implementing a process to repossess aircraft from dormant accounts.</p>

				<p>Ownership Expenses Fee - 1% of Aircraft's current buyback-value. The Ownership Expenses Fees will simulate the fixed costs of commercial aircraft ownership such as insurance, tie-down/hangar space, AIRAC subscriptions, registration fees, routine maintenance not included in the 100-hr, such as oil, tires, etc., and other costs associated with operating an aircraft in a commercial service manner. Individual line items for each of those expenses (insurance, registration, etc.) will not be broken out. Instead, a single fee will be charged each month to simulate the total fixed-cost of ownership.</p>

				<p>Aircraft Repossession: FSE is also implementing an aircraft repossession process to remove aircraft from dormant accounts and return them to the available pool of aircraft. Aircraft will be removed from player and group accounts after 36 consecutive months of non-payment of the Ownership Expenses Fee. The dormant account will receive a cash compensation at the current buyback value of the aircraft, minus recovered debt.</p>

				<p>For more information, see the forums announcement and/or the FSE User Manual:</p>

				<ul>
					<li><a href="http://www.fseconomy.net/forum/fse-anouncements/37712-fse-major-update-aircraft-ownership-expenses-fee-repossession">Announcement</a></li>
					<li><a href="https://sites.google.com/site/fseoperationsguide/aircraft-details/aircraft-cost-of-ownership#TOC-Aircraft-Ownership-Expenses-Fee">FSE User Manual</a></li>
				</ul>
			</div>
		</section>
<% 	}
%>
	<!-- END NEW ITEM -->

	<!-- NEWS ITEM Logged In Only -->
<%
	if (user.isLoggedIn()) 
	{
%>
		<section>
			<div>
				<h3>Update - Building FBOs</h3>
				<p>There is a new requirement for building New FBO's. You must have at least 10kg of supplies on site to build, if you have negative supplies at that location you must bring in enough supplies to have positive supplies with a minimum of 10 Kg to build.</p>
			</div>
			<div>
				<h3>FSE Reporter - Issue Released!</h3>
				<p><img src="https://sites.google.com/site/fsereporter/home/cover.png" style="width: 100px; float:left;margin:0 10px" /></p>
				<p>The FSE Reporter is FSE's official monthly publication. A wealth of content published by fellow FSE members, and about FSE members, is waiting for your perusal. Get paid to contribute! Advertise your flying group or business! See the FSE Reporter website for details: <a href="https://sites.google.com/site/fsereporter/">https://sites.google.com/site/fsereporter/</a></p>
			</div>
		</section>
<% 	} 
%>
	<!-- END NEW ITEM -->
	
	
	<!-- NEWS ITEM Logged In Only -->
<%
	if (user.isLoggedIn()) 
	{
%>
		<section>
			<div>
				<h3>Player/Group Name Entry Fields</h3>
				<p>In an effort to both speed up page loading times and provide some new efficiencies to our users, FSEconomy has adjusted how players interact with various entry forms. Developers have spent time modifying how you enter the name of who you wish to transfer funds, FBO's, airplanes, and goods to.</p>
				<p>Instead of providing a drop-down selection box with ALL users and groups listed, you are now presented with a text box where you will simply type the user or group name. After several characters are entered, you will be presented with the first ten autocompletion options to choose from - you can a) select one from the list, b) continue typing to refine the autocomplete options, or c) type all the way to completion. You can also copy/paste names into the field if you wish.</p>
				<p>Very important: you MUST click on a name in the list, even if you type the entire name or paste it in.</p>
			</div>
		</section>
<% 	} 
%>
	<!-- END NEW ITEM -->
	
	
	<!-- NEWS ITEM Logged In Only -->
<%
	if (user.isLoggedIn()) 
	{
%>
		<section>
			<div>
				<h3>All-In Airliner Testing Continues...</h3>
				<p>FSEconomy has introduced the <b>Boeing 737-800</b> airframe to the testing process for All-In airline style jobs in FSEconomy. The 737-800 is a very popular airplane in the simulation world, owing this reputation to several world-class aircraft addons. Jobs are popping up all over the globe, so be certain to take advantage!</p>
			</div>
		</section>
<% 	} 
%>
	<!-- END NEW ITEM --> 
	
	<!-- NEWS ITEM Logged In Only -->
<%
	if (user.isLoggedIn()) 
	{
%>
		<section>
			<div>
				<h3>Bulk Fuel Ordering Changes</h3>
				<p>In coordination with the recent allowance for all airports to receive bulk fuel, the FSEconomy coding staff has been hard at work refining this process. Effective immediately, several important changes have been made to the bulk fuel ordering process. Key highlights include limitations on who can buy wholesale fuel from the FSEconomy suppliers, bulk fuel delivery delays, and limits to the numbers of orders per day have all been implemented to further enhance the depth of FSEconomy. </p>
			</div>
		</section>
<% 	} 
%>
	<!-- END NEW ITEM --> 
	
	<!-- NEWS ITEM Logged In Only -->
<%
	if (user.isLoggedIn()) 
	{
%>
		<section>
			<div>
				<h3>August Changes</h3>
				<p> The FSEconomy coders have been hard at work through the month of August, making several important changes to bring both balance and new features to the FSE world.</p>
				<h4> Have you seen the Boeing 727's yet?</h4>
				<p> FSEconomy is currently <i>testing</i> the inclusion of an airliner as an expansion of the "All-In" job types recently introduced. The FSE world has been seeded with several Boeing 727-100 aircraft, and they are exclusively associated with all-in jobs designed for them. The project is currently in its <b>public beta</b> phase, and is subject to change over time as it is refined. Use the airport search features to find the 727's and their jobs if you'd like to take one for a flight!</p>
				<h4> Bank of FSE Interest Adjustment</h4>
				<p> In an effort to better balance the economics of inflation within FSEconomy, the Board of Directors has opted to change how bank interest is paid. Personal bank balances up to and including $1,000,000 will continue to earn daily interest as they have in the past. Balances over $1M will only earn interest on the first million dollars saved in the bank. Group bank accounts will no longer generate interest.</p>
				<h4> Airplane Sellback Changes</h4>
				<p> Finally, as a new game dynamic, aircraft "system sellback" prices now have a depreciation modifier applied to them. The system sellback price will decrease from 70% of the base price at 00:00 airframe hours steadily down to a minimum of 25% of the base price at 10,000 hours.</p>   
			</div>
		</section>
<% 	} 
%>
	<!-- END NEW ITEM --> 
	
	
	<!-- NEWS ITEM Logged In Only -->
<%
	if (user.isLoggedIn()) 
	{
%>
		<section>
			<div>
				<h3>The Return of "All-In" Jobs</h3>
				<p>FSEconomy has reintroduced the concept of pilot-for-hire (aka: all-in) jobs. These jobs are based on the concept of being hired as a charter pilot for the FSE system itself. The system will have selected a job, selected an airplane, and prepared compensation for the safe completion of the flight. All you have to do is fly it! All-in jobs will pop up at chosen airports and be assigned a specific airplane which matches the job's requirements.</p>
				<p>Simply add the all-in job to your assignment queue and rent the associated airplane to fly one! Remember, with all-in jobs, you are not the boss! Because of this, any other assignments in your "My Flights" page will need to stay on the ground, and only the all-in job should board the designated airplane. That being said, the cost of fuel, rental charges and additional crew fees will all be taken care of for you by "the boss". Once your job is completed you will be paid your job wages minus the <em>very</em> modest ground crew fees which might be owed to local FBO owners.</p>
			</div>
		</section>
<% 	} 
%>
	<!-- END NEW ITEM --> 
	
	
	<!-- NEWS ITEM Logged In Only -->
<%
	if (user.isLoggedIn()) 
	{
%>
		<section>
			<div>
				<h3>Visit the Forums!</h3>
				<p>When asked why they continue to fly with FSEconomy, many of our pilots responded that the <em><a href="http://www.fseconomy.net">community</a></em> is just as much of a part of their FSE enjoyment as the flying.</p>
				<p>The FSE Forums are a great resource for buying and selling airplanes and FBO's, tips and tricks to maximize your enjoyment in FSE, and finding groups and organizations which you can join to further your experience! You can also get up-to-the-minute updates on any server issues, system enhancements and changes which may take place. </p>
				<p>Most of all, the FSE forums are the lifeblood of the FSE community. <b>Don't be shy</b>! Stop on by, take a look, and discover the "hidden" reason why FSEconomy can be such a fun and addicting extension for your flight simulation experience!</p>
			</div>
		</section>
<% 	} 
%>
	<!-- END NEW ITEM --> 
	
	<!-- NEWS ITEM Logged In Only -->
<%
	if (user.isLoggedIn()) 
		{
%>
		<section>
			<div>
				<h3>Donations:</h3>
				<p>Coming soon!</p>
			</div>
		</section>
<% 	} 
%>
	<!-- END NEW ITEM --> 
		

	</div> <!-- ends news area-->
	</div> <!-- ends content -->
	<!-- MAIN PAGE FOOTER  ======================================================= -->
	<div class="message footer">
		<div>
			<p>FSEconomy is managed by an appointed Board of Directors and all members of our community agree to abide by the FSEconomy <a href="http://www.fseconomy.net/tos" target='_blank'>Etiquette and Rules of Fair Play</a> whenever they log on to the server.</p>
			<p>&copy; Copyright 2005-2020, <a href="#" onclick="team.setWindowProperties('toolbar=no,location=no,status=no,menubar=no,scrollbars=yes,resizable=no,alwaysRaised,dependent,titlebar=no');team.setSize(400,325);team.setUrl('<%= response.encodeURL("team.htm")%>');team.showPopup('team');return false;" id="team">FSE Development Team</a>. You can redistribute and/or modify this source code under the terms of the <a href="gnu.jsp">GNU General Public License</a> as published by the Free Software Foundation; either version 2 of the License, or any later version.&nbsp; FSEconomy and its authors must be publicly credited on any site that uses this source code or work derived from this source code. </p>
			<p>FSEconomy development is supported by Open Source Licenses for their products by
			<a href="https://www.atlassian.com/"><img src="img/atlassian-rgb-navy-xsmall.png"></a>
			and
			<a href="https://www.jetbrains.com/"><img src="img/logo_jetbrains.png"></a>
			</p>
		</div>
	</div>
</div> <!-- ends wrapper -->
</body>
</html>